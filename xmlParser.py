import os
from lxml import etree
import getopt
import sys
import time

__author__ = "profs89"


def get_os_param(param):
    if param == "slash":
        if os.name == 'nt':
            return "\\"
        else:
            return "/"

    if param == "space_replace":
        if os.name == 'nt':
            return " "
        else:
            return "\ "

    if param == "quote":
        if os.name == 'nt':
            return "\""
        else:
            return ""


def print_params(folder, document_root_tag, child_tag, document_numbers, acceptable_formats, attributes):
    print "==== Params ===="
    print "folder = " + folder
    print "document root tag = [" + "/".join(document_root_tag) + "]"
    print "child tag = [" + ";".join(child_tag) + "]"
    print "document numbers = [" + ";".join(document_numbers) + "]"
    print "acceptable formats = [" + ";".join(acceptable_formats) + "]"
    print "attributes = [" + ";".join(attributes) + "]"
    print "================"


def parse_xml_file(file_name, folder, document_root_tag, child_tag, attributes, document_numbers, documents, slash):

    parser = etree.XMLParser(strip_cdata=False)
    in_tree = etree.parse(folder + slash + file_name, parser)
    in_root = in_tree.getroot()

    for document in in_root.findall(document_root_tag[1]):
        contains = 0
        if len(child_tag) is not 0:
            for child in child_tag:
                if document.find(child).text in document_numbers:
                    contains += 1

        if len(attributes) is not 0:
            for attribute in attributes:
                data = attribute.split("@")
                tag = data[0]
                data2 = data[1].split("=")
                attr_name = data2[0]
                attr_value = data2[1]
                if tag == document_root_tag[1]:
                    if document.get(attr_name) == attr_value:
                        contains += 1
                else:
                    if document.find(tag).get(attr_name) == attr_value:
                        contains += 1

        if contains == len(child_tag) + len(attributes) and len(child_tag) + len(attributes) is not 0:
            documents.append(document)


def run(argv):
    opts, argv = getopt.getopt(argv, "f:r:c:d:a:v:h",
                               ["--folder", "--root", "--child", "--documents", "--attributes", "--verbose", "--help"])
    folder = "."
    document_root_tag = ""
    child_tag = ""
    verbose = None
    show_help = None
    document_numbers = ""
    acceptable_formats = ["xml", "XML"]
    attributes = ""

    # Parse arguments
    for opt, arg in opts:

        if opt in ('-f', '--folder'):
            folder = arg

        if opt in ('-r', '--root'):
            document_root_tag = arg.split("/")

        if opt in ('-c', '--child'):
            child_tag = arg.split(";")

        if opt in ('-d', '--documents'):
            document_numbers = arg.split(";")

        if opt in ('-a', '--attributes'):
            attributes = arg.split(";")

        if opt in ('-v', '--verbose'):
            verbose = True

        if opt in ('-h', '--help'):
            show_help = True

    if show_help:
        print "Command description:\n" \
            "-f or --folder - specifies location of xml files\n" \
            "-r or --root - specifies root tag of xml document\n" \
            "-c or --child - specifies child tag of xml document\n" \
            "-d or --documents - specifies document numbers to search for in the xml files\n" \
            "-a or --attributes - it is possible to search by atributes in xml tags\n" \
            "-v or --verbose - verbose mode to show logs\n" \
            "-h or --help - to show this help\n" \

        print "Command samples: \n" \
            "python parseXML.py -r LetterInstallment/LetterFile -c Letter/Code;Letter/Language -d 531;EN;FI;SV " \
            "-f /home/path\n\n" \
            "python parseXML.py -r EmuFile/Invoice -c MainInvoiceInfo-101/InvoiceNo -d 010557816 " \
            "-f /home/path\n\n" \
            "python parseXML.py -r EmuFile/Invoice -c MainInvoiceInfo-101/InvoiceNo;MainInvoiceInfo-101/ProcessCode " \
            "-d 010557816;RP -f /home/path\n\n" \
            "python parseXML.py -r Fibos/Document -c Body/ReferencedInvoiceNo " \
            "-d 40262366;78580110;26297449;26297495;53842159;26297614;53887331;53863304;78403911;53851995;26297692;" \
            "53863311;53884520;78588734;78589573;78573262;78568304 -f /home/path\n\n"

    else:
        print_params(folder, document_root_tag, child_tag, document_numbers, acceptable_formats, attributes)

        slash = get_os_param("slash")

        documents = etree.Element(document_root_tag[0])

        if folder[-1] == slash:
            folder = folder[:-1]

        for in_file in os.listdir(folder):
            name = in_file.split(".")
            if len(name) > 1:

                if name[len(name)-1] in acceptable_formats:
                    print "Parsing file - " + in_file
                    parse_xml_file(in_file, folder, document_root_tag, child_tag, attributes, document_numbers, documents, slash)

        directory_name = folder + slash + "parsed_data" + slash

        if not os.path.exists(directory_name):
            os.makedirs(directory_name)

        tree = etree.ElementTree(documents)
        save_name = "%s%f%s" % (directory_name + "data", time.time(), ".xml")
        tree.write(save_name, encoding="UTF-8")


if __name__ == "__main__":
    run(sys.argv[1:])